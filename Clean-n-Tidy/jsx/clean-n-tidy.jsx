var types = { CLIP: 1, BIN: 2, ROOT: 3, FILE: 4 };
// TODO localise these bois:
var UNUSED_BIN_NAME = "_Unused Items_";
var RESTORED_BIN_NAME = "_Restored Items_";

function isInArr(n, arr) {
    if (arr) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === n) {
                return true;
            }
        }
    }
    return false;
}

function getAllProjectItems(topLevel) {
    var itms = [];
    for (var i = 0; i < topLevel.children.length; i++) {
        itms.push(topLevel.children[i]);
        if (topLevel.children[i].type === types.BIN) {
            itms = itms.concat(getAllProjectItems(topLevel.children[i]));
        }
    }
    return itms;
}

function preserveSelectedItems(preserveSelected) {
    var itms = [];
    var theSelected = app.getCurrentProjectViewSelection();

    if (theSelected && preserveSelected) {

        for (var s = 0; s < theSelected.length; s++) {
            app.setSDKEventMessage("Clean-n-Tidy: adding " + theSelected[s].name + "to preserved items list", 'info');
            itms.push(theSelected[s].nodeId);
            if (theSelected[s].type === types.BIN) {
                var selectedChildren = getAllProjectItems(theSelected[s]);
                for (var c = 0; c < selectedChildren.length; c++) {
                    itms.push(selectedChildren[c].nodeId);
                }
            }
        }
    }
    return itms;
}

function getSelectedSequences() {
    var selectedSequences = [];
    var theSelected = app.getCurrentProjectViewSelection();
    if (theSelected) {
        for (var s = 0; s < theSelected.length; s++) {
            var projItem = theSelected[s];

            if (projItem.isSequence()) {
                selectedSequences.push(projItem.nodeId);
            }
        }
    }
    return selectedSequences;
}

function seqsSelected() {
    var selectedSequences = getSelectedSequences();
    return selectedSequences.length > 0;
}

function getProjSeqs() {
    var allSeqs = (app.project.sequences);
    var seqs = [];
    for (var s = 0; s < allSeqs.length; s++) {
        seqs.push(allSeqs[s].projectItem.nodeId);
    }
    return seqs
}

function getSourceSequences(onlySelected) {
    if (onlySelected) {
        return getSelectedSequences();
    }
    return getProjSeqs();
}

function getItemsToRetrieve(bin, usedItems) {
    var itemsToRetrieve = [];
    if (bin.children) {
        for (var i = 0; i < bin.children.length; i++) {
            var item = bin.children[i];
            if (isInArr(item.nodeId, usedItems)) {
                itemsToRetrieve.push(item.nodeId);
            } else if (item.type === types.BIN) {
                itemsToRetrieve = itemsToRetrieve.concat(getItemsToRetrieve(item, usedItems));
            }
        }
    }
    return itemsToRetrieve;
}

function getItemByName(container, itemName) {
    containerItems = container.children;
    for (var i = 0; i < containerItems.length; i++) {
        if (containerItems[i].name === itemName) {
            return containerItems[i];
        }
    }
    return false;
}
function createBinIfNeeded(container, newBinName) {
    var existingBin = getItemByName(container, newBinName);
    return (existingBin) ? existingBin : container.createBin(newBinName);
}


function moveItemsWithHierarchy(container, targetBin, targetList, sense, filteredBinNames, deleteEmptyBins) {
    var count = {
        clips: 0,
        bins: 0,
        add: function (newResults) {
            this.clips += newResults.clips;
            this.bins += newResults.bins
        }
    };
    if (!isInArr(container.name, filteredBinNames)) {
        var containerItems = container.children;
        for (var i = containerItems.length - 1; i >= 0; i--) {
            // don't look in the unused bin

            if (containerItems[i].type === types.BIN) {
                if (
                    // bin name is not in the filtered list (i.e. ["_Unused_Items_"])
                    (!isInArr(containerItems[i].name, filteredBinNames)) &&
                    // bin is not in the target list, if sense is false (tidying)
                    // bin is in the target list, if sense is true (untidying)
                    (sense === isInArr(containerItems[i].nodeId, targetList))
                ) {
                    $.writeln("Moving hierarchy of " + containerItems[i].name + " to " + targetBin.name + "/" + containerItems[i].name);
                    var newTargetBin = createBinIfNeeded(targetBin, containerItems[i].name);
                    count.add(moveItemsWithHierarchy(containerItems[i], newTargetBin, targetList, sense, filteredBinNames, deleteEmptyBins));
                }
            } else if (sense === isInArr(containerItems[i].nodeId, targetList)) {
                $.writeln(containerItems[i].name + " moved to " + targetBin.name);
                containerItems[i].moveBin(targetBin);
                count.clips++;
            }

        }
        if (container.children.length === 0) {
            if (deleteEmptyBins) {
                container.deleteBin();
                count.bins++;
            }
        }
    }
    return count;
}

function getSequence(projItem) {
    for (var i = 0; i < app.project.sequences.length; i++) {
        if (app.project.sequences[i].projectItem.nodeId === projItem.nodeId) {
            return app.project.sequences[i];
        }
    }
    return false;
}

function addUsedItem(clip, usedItems) {
    if (clip.projectItem) {
        var clipId = clip.projectItem.nodeId;
        if (!isInArr(clipId, usedItems)) {
            usedItems.push(clipId);
        }
        if (clip.projectItem.isSequence()) {
            // recurse into nested sequences
            var nestedSeq = getSequence(clip.projectItem);
            if (nestedSeq) {
                usedItems = usedItems.concat(getClipsFromSequence(nestedSeq));
            }
        }
    }
    return usedItems;
}

function getClipsFromSequence(seq) {
    var usedItems = [];
    var videoTracks = seq.videoTracks;
    var audioTracks = seq.audioTracks;

    for (var j = 0; j < videoTracks.numTracks; j++) {
        var track = videoTracks[j];

        for (var k = 0; k < track.clips.numItems; k++) {
            usedItems = addUsedItem(track.clips[k], usedItems);
        }
    }
    for (var j = 0; j < audioTracks.numTracks; j++) {
        var track = audioTracks[j];

        for (var k = 0; k < track.clips.numItems; k++) {
            usedItems = addUsedItem(track.clips[k], usedItems);
        }
    }
    return usedItems;
}

function getUsedItems(selectedSequences) {
    var usedItems = [];
    var seqs = app.project.sequences;
    for (var s = 0; s < seqs.length; s++) {
        if (seqs[s].projectItem) {
            var seq = seqs[s];
            if (isInArr(seq.projectItem.nodeId, selectedSequences)) {
                usedItems = usedItems.concat(getClipsFromSequence(seq));
            }
        }
    }
    return usedItems;
}

function deleteEmptyBinsInContainer(container) {
    var count = 0;
    if (container.type === types.BIN ||
        container.type === types.ROOT) {
        var itms = container.children;
        if (itms) {
            for (var i = itms.length - 1; i >= 0; i--) {
                if (itms[i].type === types.BIN) {
                    count += deleteEmptyBinsInContainer(itms[i]);
                    if (itms[i].children.length === 0) {
                        itms[i].deleteBin();
                        count++;
                    }
                }
            }
        } else {
            container.deleteBin();
        }
    }
    return count;
}

function collapseRedundantHierarchies(topLevel, excludedBins) {
    var count = 0;
    /* collapse single bin towers e.g. structures like
    Bin1
    > Bin2
    > > bin3
    > > > [clips]
    down to:
    Bin1
    > [clips]
    */
    var kids = topLevel.children;
    // first go to the bottom of the hierarchy
    for (var i = kids.length - 1; i >= 0; i--) {
        if (kids[i].type === types.BIN && !isInArr(kids[i].nodeId, excludedBins)) {
            count += collapseRedundantHierarchies(kids[i]);
        }
    }
    // if the current bin has only a single bin as child, 
    // move its contents up to the current bin
    while (kids && kids.length === 1 && kids[0].type === types.BIN) {
        if (kids[0].children) {
            var grandKids = kids[0].children;
            for (var i = grandKids.length - 1; i >= 0; i--) {
                grandKids[i].moveBin(topLevel);
            }
        }
        kids[0].deleteBin();
        count++
        kids = topLevel.children;
    }
    return count;
}

function countBin(theBin) {
    var count = 0;
    var binChildren = theBin.children;
    if (binChildren) {
        for (var i = 0; i < binChildren.length; i++) {
            if (binChildren[i].type === types.BIN) {
                count += countBin(binChildren[i]);
            } else {
                count++;
            }
        }
    }
    return count;
}

function plural(number, phrase) {
    return "" +
        number + " " +
        ((number === 0 || number > 1) ?
            phrase :
            phrase.slice(0, -1));
}

function aspirational() {
    function a(choice) {
        var word = choice[Math.round(Math.random() * (choice.length - 1))];
        return [(word.match(/^[aeiou]/) ? "an" : "a"), word].join(" ")
    }
    var clean = ["clean", "ordered", "tidy", "organised", "uncluttered", "well structured"];
    var happy = ["calm", "happy", "efficient", "beautiful", "satisfying", "relaxing"];
    var project = "project is"
    return [a(clean), project, a(happy), project].join(" ").replace(/^a/, "A").replace(/\sis$/, ".");
}

function reduceProject(
    onlySelected,
    preserveSelected,
    deleteEmptyBins,
    collapseBinTowers,
    consolidateDuplicates,
    saveFirst,
    deleteUnused
) {
    if (saveFirst) {
        app.project.save();
        app.setSDKEventMessage('Clean-n-Tidy: Project saved before tidying up.', 'info');
    }
    var keepgoing = true;
    // ----------------------Do All The Things --------------------------
    var selectedSequences = getSourceSequences(onlySelected);
    if (selectedSequences.length === 0) {
        var inTheTarget = onlySelected ? "selected." : "in the project.";
        keepgoing = confirm(
            "There are no sequences " + inTheTarget + "\nConsolidate files, clean up folders and delete empty bins anyway?",
            false,
            "No sequences " + inTheTarget
        );
    }
    if (keepgoing) {
        var result = { clips: 0, bins: 0 };
        var clipsToRetrieve = [];
        var unusedItemsBin;
        if (consolidateDuplicates) {
            // do this first, or files can get consolidated into the _Unused items_ folder
            app.project.consolidateDuplicates();
        }
        if (selectedSequences.length > 0) {
            // the selected sequences are used items
            var usedItems = selectedSequences;
            // add any selected items if preserveSelected is true
            usedItems = usedItems.concat(preserveSelectedItems(preserveSelected));

            unusedItemsBin = createBinIfNeeded(app.project.rootItem, UNUSED_BIN_NAME);


            usedItems = usedItems.concat(getUsedItems(selectedSequences));
            clipsToRetrieve = getItemsToRetrieve(unusedItemsBin, usedItems);
            if (clipsToRetrieve.length > 0) {
                moveItemsWithHierarchy(unusedItemsBin, app.project.rootItem, clipsToRetrieve, true, [], deleteEmptyBins);
            }
            var filteredBinNames = [UNUSED_BIN_NAME];
            result = moveItemsWithHierarchy(app.project.rootItem, unusedItemsBin, usedItems, false, filteredBinNames, deleteEmptyBins);
        }
        if (deleteEmptyBins) {
            result.bins += deleteEmptyBinsInContainer(app.project.rootItem);
        }
        if (collapseBinTowers) {
            var excludedBins = [unusedItemsBin.nodeId];
            result.bins += collapseRedundantHierarchies(app.project.rootItem, excludedBins);
        }

        var msg = ((result.clips) ? "Moved " + plural(result.clips, "unused clips") + " to " + UNUSED_BIN_NAME + "\n" : "") +
            ((clipsToRetrieve.length) ? "Retrieved " + plural(clipsToRetrieve.length, "used clips") + " from " + UNUSED_BIN_NAME + ".\n" : "") +
            ((result.bins) ? "Deleted " + plural(result.bins, "empty bins") + ".\n" : "")

        if (deleteUnused) {
            var doDelete = confirm(
                msg + "\nDelete all " + plural(countBin(unusedItemsBin), "clips") + " in " + UNUSED_BIN_NAME + "?",
                false,
                "Delete unused?"
            );
            if (doDelete) {
                unusedItemsBin.deleteBin();
                app.setSDKEventMessage('Clean-n-Tidy: Unused items were deleted.\nRevert now if you want to undo!', 'warning');

            }
        }
        msg = msg ? msg : "Nothing was changed. You're a very tidy person";
        app.setSDKEventMessage("Clean-n-Tidy: " + msg, 'info');
        return (msg);
    }
    return aspirational();
}

function unReduce(saveFirst) {
    if (saveFirst) {
        app.project.save();
        app.setSDKEventMessage('Clean-n-Tidy: Project saved before un-tidying.', 'info');
    }

    var unusedItemsBin = getItemByName(app.project.rootItem, UNUSED_BIN_NAME);
    if (unusedItemsBin) {
        var result = moveItemsWithHierarchy(unusedItemsBin, app.project.rootItem, [], false, [], false);
        if (result.bins || result.clips) {
            return ("Moved " +
                (result.clips ? result.clips + " clips" : "") +
                (result.clips && result.bins ? " and " : "") +
                (result.bins ? result.bins + " bins" : "") +
                " out of " + UNUSED_BIN_NAME);

        }
        deleteEmptyBinsInContainer(unusedItemsBin);
    }
    return "Nothing was changed."
}
// reduceProject(
//     onlySelected = true,
//     preserveSelected = true,
//     deleteEmptyBins = true,
//     collapseBinTowers = true,
//     consolidateDuplicates = true,
//     saveFirst = false,
//     deleteUnused = false
// )

// unReduce(false);