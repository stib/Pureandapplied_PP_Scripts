# Clean-n-Tidy

## Like Reduce Project in After Effects, but for Premiere
### This will delete unused clips and project items from your project (but not from disk), delete empty bins, and simplify bin structures.

---

## Installing
By far the easiest way to install is using the ZXP installer from AEScripts: https://aescripts.com/learn/zxp-installer/ 

Go to the releases tab at https://codeberg.org/stib/Pureandapplied_PP_Scripts/releases/ and click on the latest release.
Scroll to the bottom of the readme, and there is a list of files you can download. Download the Clean-n-Tidy.zxp file
Run the zxp installer and use it to open the `Clean-n-Tidy.zxp` file that you just downloaded.

---

## User Manual 
### Source settings
```
Keep project items that are used by
☑ only selected sequences.
☑ any sequence.
```

If you only want to preserve clips that are referenced by particular sequences, chose the first option and select the relevant sequences in the project window. 
If you want to keep all clips used in any sequence, choose the second option. 
*Clips that are referenced in nested sequences are also kept, so be chill.*
```
☑ Preserve selected project items.
 ```
if you want to preserve an unused clip, or a bin and all its contents, select it and choose this option.

--- 
### Settings
```
☑ Save before reducing.
```
Sad fact about the rather feeble scripting API for Premiere is that there is no way to create an "undo group" like there is in AE. So if this script moves a hundred clips around, you'll need to do at least a hundred undos to get back to where you were before you ran it. Ugh.

So the best thing to do if you're unsure about what the script will do to your project is to save it first. Then if it all goes to custard you can always revert. Soz, that's just the way it is. 
There is an **Un-Tidy-Up** button, which will reverse what the script does to some extent. *Read on for more info*
```
☑ Consolidate duplicate footage items.
```
This is just the same as the native Premiere consolidate footage function, it's just here for convenience. It means if you have multiple copies of the same media file, say if you've imported it more than once, it will smush them together into one clip. This happens before any other step.
```
☑ Delete empty bins.
☑ Collapse redundant bin structures.
```
These options help tidy up your bin structures. 

☑ Delete empty bins does what it says on the tin. Any bin with nothing in it will be removed.

☑ Collapse redundant bin structures gets rid of stacks of bins that only contain one bin. So if you have a bin structure that looks like:
```
Footage
    Day 1
        Camera 1
            Clips
                Go takes
                    clip1.mov
                    clip2.mp4
                    clip3.wav
```
You will end up with
```
Footage
    clip1.mov
    clip2.mp4
    clip3.wav
```
Ah, so much nicer. Fun fact: *the Premiere scripting API is so slow you can watch the structures collapsing in real time, it's kinda cool*. Seriously though, this script can take a little bit of time in large projects. Sit on your hands and let it do its thing, the info text at the bottom will keep you informed.

Note that the collapse function does not apply to the `_Unused_Items_` bin, see below for why.
 ```
☑ Move unused to bin.
☐ Delete unused from project.
```
- If you chose the first option, all files are moved to a bin called `_Unused_Items_`. 
Bin structure is preserved, because I wanted to make things hard for myself, and because it makes it possible to put everything back later—see below.
- If you choose the second option the clips are removed from the project. They are **not** made offline or deleted from disk, that's for Media Manager. This tool is just for cleaning up your project. For now…
---
### The actual buttons that do the things
```
[Tidy-Up]
```
 - Hitting `Tidy-Up` will do All The Things. depending on the settings above.
 ```
 [Un-Tidy-Up]
 ```
 - Hitting `Un-Tidy-Up` will put the items in the `_Unused_Items_` bin back into the top level bin of the project, preserving the bin structure, and merging the bins with existing ones in the project. This means that if you haven't moved things around in the `Unused_Items_` bin, or changed the bin structure in the rest of your project they'll go back to where they were originally. *This is why redundant bin structures aren't collapsed in the `Unused_Items_` bin, so that the source structure is preserved.*

---

## This script is Free software
It is released under the GPL 3 license, go look that up here: https://www.gnu.org/licenses/gpl-3.0.en.html 
UI font is Ubuntu, which is released under the UFL, which can be found here https://ubuntu.com/legal/font-licence

## More free stuff
I write these scripts because I like the challenge—this was a learning curve for me, I've never written a script for PPro before, only After Effects. My blog and scripts can be found here: https://blob.pureandapplied.com.au 

## Contributing
If you want to contribute to more scripts, hit me up at https://aus.social/@stib and tell me you like it, then donate something to someone who deserves a donation. Or help with some coding; all the souce code for all the scripts is at https://codeberg.org/stib/

