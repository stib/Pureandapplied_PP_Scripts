(function () {
	'use strict';
	// ----------------------------------------------------------------
	let extensionName = "CleanNTidy";
	let extensionVersion = "0.1.3";
	let cookieName = extensionName + extensionVersion;
	// ----------------------------------------------------------------
	/* Create an instance of CSInterface. */
	var csInterface = new CSInterface();
	themeManager.init();
	// ----------------------------------------------------------------
	setInterval(getSeqs, 2000);
	var reduceBtn = document.getElementById("reduceBtn");
	reduceBtn.addEventListener("click", reduce);
	var unReduceBtn = document.getElementById("unReduceBtn");
	unReduceBtn.addEventListener("click", unReduce);

	let ctrls = [
		'onlySelected',
		'allSeq',
		'preserveSelected',
		'deleteEmptyBins',
		'collapseBinTowers',
		'consolidateDuplicates',
		'saveFirst',
		'deleteUnused',
		'keepUnused'
	];
	initialiseUI();
	getSeqs();
	info(extensionName + " " + extensionVersion);

	function info(msg) {
		document.getElementById('info').innerHTML = msg;
	}

	/* Write a helper function to pass instructions to the ExtendScript side. */
	function reduce() {
		// info( "Reducing the project");
		document.getElementById('reduceBtn').disabled = true;
		const settings = getSettings();
		const scrpt = `reduceProject(${[
			settings.onlySelected,
			settings.preserveSelected,
			settings.deleteEmptyBins,
			settings.collapseBinTowers,
			settings.consolidateDuplicates,
			settings.saveFirst,
			settings.deleteUnused
		].join(', ')})`;
		csInterface.evalScript(scrpt, (result) => {
			info(result.replace(/\n/, "<br>"));
		});
		document.getElementById('reduceBtn').disabled = false;

	}
	/* Make a reference to your HTML button and add a click handler. */
	/* Make a reference to your HTML button and add a click handler. */

	function unReduce() {
		info("un-Reducing the project");
		const scrpt = 'unReduce()';
		csInterface.evalScript(scrpt, (result) => {
			info(result.replace(/\n/, "<br>"));
		});
	}

	function getSeqs() {
		csInterface.evalScript('seqsSelected()', (result) => {
			// alert(result);
			var cantTidy = (result === 'false') && document.getElementById('onlySelected').checked
			document.getElementById("reduceBtn").disabled = cantTidy;
			document.getElementById("reduceBtn").title = cantTidy ?
				"Can't reduce: select at least one sequence,\nor check the 'all sequences button'" :
				"Reduce the project to only necessary clips"
		});
	}


	function setCookie(cname, cvalue, exdays) {
		const d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		let expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	function getCookie(cname) {
		let name = cname + "=";
		let ca = document.cookie.split(';');
		for (let i = 0; i < ca.length; i++) {
			let c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	function getSettings() {
		let settings = {}
		for (let c in ctrls) {
			settings[ctrls[c]] = document.getElementById(ctrls[c]).checked;
		}
		return settings;
	}

	function setControls(settingsStr) {
		if (settingsStr) {
			let settings = JSON.parse(settingsStr);
			if (settings) {
				for (let c in ctrls) {
					if (settings.hasOwnProperty(ctrls[c])) {
						document.getElementById(ctrls[c]).checked = settings[ctrls[c]];
					}
				}
			}
		}
	}


	function saveSettings() {
		let prefs = JSON.stringify(getSettings());
		setCookie(
			cookieName,
			prefs,
			365
		)
	}

	function initialiseUI() {
		let settings = getCookie(cookieName);
		console.log("Checking cookie: " + settings);
		if (settings != "" && settings != null) {
			setControls(settings);
		} else {
			setCookie(cookieName, JSON.stringify(getSettings()), 365);
		}

		for (let c in ctrls) {
			document.getElementById(ctrls[c]).addEventListener('click', saveSettings);
		}
		document.getElementById('onlySelected').addEventListener('click', getSeqs);
		document.getElementById('allSeq').addEventListener('click', getSeqs);
	}

}());