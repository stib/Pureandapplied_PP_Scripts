var sourceFolder = Folder.selectDialog("Choose the source folder for the replacement media");

if (sourceFolder.exists) {
    var selectedClips = app.getCurrentProjectViewSelection();
    for (var s = 0; s < selectedClips.length; s++) {
        var theClip = selectedClips[s];
        // check to see that item is a clip
        if (theClip.type === 1 && theClip.canChangeMediaPath()) {
            replaceWithMatchingClip(theClip, sourceFolder);
        }
    }

}
function replaceWithMatchingClip(theClip, theFolder) {
    var theContents = theFolder.getFiles();
    var theClipMedia = theClip.getMediaPath();
    if (theClipMedia) {
        var f = new File(theClipMedia);
        var clipName = f.name.replace(/\.[^.]+$/, "");
        var keepSearching = true;
        for (var i = 0; i < theContents.length && keepSearching; i++) {
            var clipnameRegex = new RegExp(clipName + "\\.[^.]+$", "i");
            if (theContents[i].name.match(clipnameRegex)) {
                $.writeln("Replacing " + theClipMedia + " with " + theContents[i].fsName);
                theClip.changeMediaPath(theContents[i].fsName);
                keepSearching = false;
            }
        }
    }
}
