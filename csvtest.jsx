// csv file should look like this:
//    File Name,In Point, Out Point
//    V:\path\to\media\file\filename_1.mov,01:23:45:06,01:23:46:00
//    V:\path\to\media\file\filename_foo.mov,01:23:45:06,01:23:46:00
var csvFile = File.openDialog("Select the CSV file", "Comma Separated Values: *.csv");

if (csvFile) {
    csvFile.open("r")
    var csvContent = csvFile.read();
    if (csvContent) {
        var lines = csvContent.split("\n");
        var header = lines[1].split(",");

        var inPointIndex = 1;//header.indexOf("In Point"); //<-this JS doesn't work in ES
        var outPointIndex = 2; //header.indexOf("Out Point");
        var fileNameIndex = 0; //header.indexOf("File Name");

        var timecodeFormat = /^\d{2}:\d{2}:\d{2}:\d{2}$/;

        var app = new Object();
        app.project.createNewSequence("Sequence from CSV", "HDV 720 25"); //might want to change this
        var sequence = app.project.activeSequence;

        for (var i = 1; i < lines.length; i++) {
            var values = lines[i].split(",");

            if (values.length == header.length && timecodeFormat.test(values[inPointIndex]) && timecodeFormat.test(values[outPointIndex])) {
                var inPoint = values[inPointIndex];
                var outPoint = values[outPointIndex];
                var fileName = values[fileNameIndex];

                var mediaFile = new File(fileName);

                if (mediaFile.exists) {
                    var clip = sequence.importMedia(mediaFile, false); //this is wrong, 
                    // ***************haven't got any further**************
                    var clipStart = sequence.getInPoint();
                    var clipEnd = sequence.getOutPoint();

                    var startTime = timeToCurrentFormat(inPoint, sequence.frameRate);
                    var endTime = timeToCurrentFormat(outPoint, sequence.frameRate);

                    clip.start = startTime;
                    clip.end = endTime;

                    sequence.insertClip(clip, startTime);
                }
            }
        }
        
    }
}

function timeToCurrentFormat(timecode, frameRate) {
    var timeArray = timecode.split(":");

    var hours = parseInt(timeArray[0]);
    var minutes = parseInt(timeArray[1]);
    var seconds = parseInt(timeArray[2]);
    var frames = parseInt(timeArray[3]);

    return (hours * 3600 + minutes * 60 + seconds + frames / frameRate) * frameRate;
}
